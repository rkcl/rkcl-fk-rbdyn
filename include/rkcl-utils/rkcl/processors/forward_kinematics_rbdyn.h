/**
 * @file forward_kinematics_rbdyn.h
 * @author Sonny Tarbouriech (LIRMM), Benjamin Navarro (LIRMM)
 * @brief Defines a utility class to compute forward kinematics based on RBDyn
 * @date 30-01-2020
 * License: CeCILL
 */

#pragma once

#include <rkcl/data/robot.h>
#include <rkcl/processors/forward_kinematics.h>
#include <rkcl/data/fwd_decl.h>
#include <rkcl/data/crtp.h>

#include <RBDyn/parsers/common.h>

#include <memory>
#include <string>

namespace rkcl
{

/**
 * @brief A utility class to compute the robot's forward kinematics using the RBDyn library
 */
class ForwardKinematicsRBDyn : virtual public ForwardKinematics
{
public:
    /**
	 * @brief Construct a new FK object
	 * @param robot reference to the shared robot
	 * @param model the robot model
	 * @param input indicates if model is a file path or a description
	 */
    ForwardKinematicsRBDyn(
        Robot& robot,
        const std::string& model,
        rbd::parsers::ParserInput input = rbd::parsers::ParserInput::File);

    /**
	 * @brief Configure the forward kinematic with the given robot and YAML configuration node.
	 * @param robot               a reference to the shared robot
	 * @param configuration       The YAML node containing the configuration.
	 */
    ForwardKinematicsRBDyn(
        Robot& robot,
        const YAML::Node& configuration);

    /**
	 * @brief Configure the forward kinematic with the given robot and YAML configuration node.
	 * Accepted values are: 'world_pose' (with a 6D vector) and 'fixed_links' (a list of links)
	 * @param configuration The YAML node containing the configuration.
	 * @return true on success, false otherwise
	 */
    virtual bool configure(const YAML::Node& configuration) override;

    /**
     * @brief Destroy the FK object
     */
    ~ForwardKinematicsRBDyn();

    /**
     * @brief Update the model with the current joint state and process the pose and Jacobian for each observation/control point.
     * @return true on success, false otherwise
     */
    bool process() override;

    void updateModel() override;

    /**
	 * @brief Compute the collision avoidance constraints from the vector of robot collision objects given in parameters
	 * @param rco_vec the vector of robot collision objects
	 */
    void computeCollisionAvoidanceConstraints(const std::vector<RobotCollisionObjectPtr>& rco_vec) override;

    // std::vector<std::vector<Eigen::MatrixXd>> computeSampledRobotCollisionObjectJacobians(const std::vector<RobotCollisionObjectPtr>& rco_vec) override;

    /**
	 * @brief Create robot collision objects from the description model and add them to the vector passed in parameter
	 * @param rco_vec a reference to the vector in which the robot collision objects are added.
	 */
    void createRobotCollisionObjects(std::vector<RobotCollisionObjectPtr>& rco_vec) override;
    /**
	 * @brief Update the pose of the robot collision objects passed in parameters
	 * @param rco_vec a reference to the vector in which the robot collision objects's pose should be updated
	 */
    void updateRobotCollisionObjectsPose(std::vector<RobotCollisionObjectPtr>& rco_vec) override;
    /**
	 * @brief Update the pose of the world collision objects passed in parameters
	 * @param wco_vec a reference to the vector in which the world collision objects's pose should be updated
	 */
    void updateWorldCollisionObjectsPose(std::vector<CollisionObjectPtr>& wco_vec) override;

    void computeGuidanceTaskJacobian(ControlPointPtr guidance_control_point, RobotCollisionObjectPtr guidance_rco) override;

    /**
	* @brief Create and add a fixed link to the robot model
	*
	* @param link_name name given to the new link
	* @param parent_link_name name of the existing link on which we want to attach the new link
	* @param joint_name name given to the new joint attaching the two links
	* @param pose pose of the new link in the parent link frame
	*/
    void addFixedLink(const std::string& link_name, const std::string& parent_link_name, const Eigen::Affine3d& pose) override;
    /**
	 * @brief Set the pose of the given fixed link
	 *
	 * @param link_name name of the link to move
	 * @param pose pose of the link in its parent link frame
	 */
    void setFixedLinkPose(const std::string& link_name, const Eigen::Affine3d& pose) override;

    /**
     * @brief Get the pose of any link in any other link frame
     *
     * @param link_name name of the link to get the pose of
     * @param reference_link_name name of the link to express the pose into
     * @return Eigen::Affine3d the link pose in the given reference frame
     */
    Eigen::Affine3d getLinkPose(const std::string& link_name, const std::string& reference_link_name = "world") const override;

private:
    /**
	 * @brief Structure pointer to implementation
	 *
	 */
    struct pImpl;
    std::unique_ptr<pImpl> impl_; //!< pointer to implementation
};

} // namespace rkcl
