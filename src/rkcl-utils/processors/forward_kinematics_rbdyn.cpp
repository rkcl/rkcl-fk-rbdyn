/**
 * @file forward_kinematics_rbdyn.cpp
 * @author Sonny Tarbouriech (LIRMM), Benjamin Navarro (LIRMM)
 * @brief Defines a utility class to compute forward kinematics based on RBDyn
 * @date 30-01-2020
 * License: CeCILL
 */
#include <rkcl/processors/forward_kinematics_rbdyn.h>

// RBDyn
#include <RBDyn/Body.h>
#include <RBDyn/FK.h>
#include <RBDyn/FV.h>
#include <RBDyn/FA.h>
#include <RBDyn/Jacobian.h>
#include <RBDyn/Joint.h>
#include <RBDyn/MultiBody.h>
#include <RBDyn/MultiBodyConfig.h>
#include <RBDyn/MultiBodyGraph.h>
#include <RBDyn/parsers/urdf.h>
#include <RBDyn/parsers/yaml.h>

#include <pid/rpath.h>
#include <yaml-cpp/yaml.h>

#include <iostream>

using namespace rkcl;

namespace
{
void parseFrame(const YAML::Node& frame,
                const std::string& name,
                Eigen::Vector3d& xyz,
                Eigen::Vector3d& rpy)
{
    xyz.setZero();
    rpy.setZero();
    if (frame)
    {
        auto xyz_node = frame["xyz"];
        if (xyz_node)
        {
            auto xyz_data = xyz_node.as<std::vector<double>>();
            if (xyz_data.size() != 3)
            {
                throw std::runtime_error("YAML: Invalid array size (" + name + "->intertial->frame->xyz");
            }
            std::copy_n(std::begin(xyz_data), 3, xyz.data());
        }
        auto rpy_node = frame["rpy"];
        if (rpy_node)
        {
            auto rpy_data = rpy_node.as<std::vector<double>>();
            if (rpy_data.size() != 3)
            {
                throw std::runtime_error("YAML: Invalid array size (" + name + "->intertial->frame->rpy");
            }
            std::copy_n(std::begin(rpy_data), 3, rpy.data());
        }
    }
}

Eigen::Matrix3d MatrixFromRPY(double r, double p, double y)
{
    Eigen::Quaterniond q = Eigen::AngleAxisd(r, Eigen::Vector3d::UnitX()) * Eigen::AngleAxisd(p, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(y, Eigen::Vector3d::UnitZ());
    Eigen::Matrix3d m;
    m = q;
    return m.transpose().eval();
}

bool parseGeometry(const YAML::Node& geometry, rbd::parsers::Geometry& data)
{
    bool has_geometry = false;
    data = rbd::parsers::Geometry();
    if (geometry)
    {
        for (YAML::const_iterator geometry_type = geometry.begin(); geometry_type != geometry.end(); ++geometry_type)
        {
            auto type = geometry_type->first.as<std::string>("");
            if (type == "mesh")
            {
                data.type = rbd::parsers::Geometry::Type::MESH;

                const auto& mesh = geometry_type->second;
                auto mesh_data = rbd::parsers::Geometry::Mesh();
                try
                {
                    mesh_data.filename = mesh["filename"].as<std::string>();
                }
                catch (...)
                {
                    throw std::runtime_error("YAML: a mesh geometry requires a filename field.");
                }
                mesh_data.scale = mesh["scale"].as<double>(1.);
                has_geometry = true;
                data.data = mesh_data;
            }
            else if (type == "box")
            {
                data.type = rbd::parsers::Geometry::Type::BOX;

                const auto& box = geometry_type->second;
                auto box_data = rbd::parsers::Geometry::Box();
                try
                {
                    box_data.size = Eigen::Vector3d(box["size"].as<std::vector<double>>().data());
                    if (box_data.size.size() != 3)
                    {
                        throw std::runtime_error("YAML: Invalid box size, should have 3 components (x, y, z)");
                    }
                }
                catch (...)
                {
                    throw std::runtime_error("YAML: a box geometry requires a size field.");
                }
                has_geometry = true;
                data.data = box_data;
            }
            else if (type == "cylinder")
            {
                data.type = rbd::parsers::Geometry::Type::CYLINDER;

                const auto& cylinder = geometry_type->second;
                auto cylinder_data = rbd::parsers::Geometry::Cylinder();
                try
                {
                    cylinder_data.radius = cylinder["radius"].as<double>();
                    cylinder_data.length = cylinder["length"].as<double>();
                }
                catch (...)
                {
                    throw std::runtime_error("YAML: a cylinder geometry requires radius and length fields.");
                }
                has_geometry = true;
                data.data = cylinder_data;
            }
            else if (type == "sphere")
            {
                data.type = rbd::parsers::Geometry::Type::SPHERE;

                const auto& sphere = geometry_type->second;
                auto sphere_data = rbd::parsers::Geometry::Sphere();
                try
                {
                    sphere_data.radius = sphere["radius"].as<double>();
                }
                catch (...)
                {
                    throw std::runtime_error("YAML: a sphere geometry requires a radius field.");
                }
                has_geometry = true;
                data.data = sphere_data;
            }
            else if (type == "superellipsoid")
            {
                data.type = rbd::parsers::Geometry::Type::SUPERELLIPSOID;

                const auto& superellipsoid = geometry_type->second;
                auto superellipsoid_data = rbd::parsers::Geometry::Superellipsoid();
                try
                {
                    superellipsoid_data.size = Eigen::Vector3d(superellipsoid["size"].as<std::vector<double>>().data());
                    if (superellipsoid_data.size.size() != 3)
                    {
                        throw std::runtime_error("YAML: Invalid superellipsoid size, should have 3 components (x, y, z)");
                    }
                    superellipsoid_data.epsilon1 = superellipsoid["epsilon1"].as<double>();
                    superellipsoid_data.epsilon2 = superellipsoid["epsilon2"].as<double>();
                }
                catch (...)
                {
                    throw std::runtime_error("YAML: a sphere geometry requires size, epsilon1 and epsilon2 fields.");
                }
                has_geometry = true;
                data.data = superellipsoid_data;
            }
            else
            {
                throw std::runtime_error("YAML: unkown geometry type '" + type + "'.Supported geometries are mesh, box, cylinder and sphere.");
            }
        }
    }
    return has_geometry;
}

} // namespace

struct ForwardKinematicsRBDyn::pImpl
{

    pImpl(Robot& robot, const std::string& model, rbd::parsers::ParserInput input)
        : robot(robot),
          forward_velocity_enabled(false),
          forward_acceleration_enabled(false),
          jacobian_derivative_enabled(false)
    {
        rbd::parsers::ParserResult parser_result;
        if (input == rbd::parsers::ParserInput::File)
        {
            auto file = PID_PATH(model);
            parser_result = rbd::parsers::from_file(file);
        }
        else
        {
            const auto robot_pos = model.find("robot");
            if (robot_pos == std::string::npos)
            {
                throw std::runtime_error("The provided robot description doesn't contain a robot node");
            }

            if ((robot_pos > 0) and (model[robot_pos - 1] == '<'))
            {
                parser_result = rbd::parsers::from_urdf(model);
            }
            else if ((model.size() > robot_pos + 5) and (model[robot_pos + 5] == ':'))
            {
                parser_result = rbd::parsers::from_yaml(model);
            }
            else
            {
                throw std::runtime_error("The description is neither a valid URDF or YAML one, no '<' before or ':' after 'robot'");
            }
        }

        mb = parser_result.mb;
        mbc = parser_result.mbc;
        mbg = parser_result.mbg;
        collision_map = parser_result.collision;

        addWorldToKinematicModel();

        checkJointTypes();

        jointGroupIndices();

        initRobotJointLimits(parser_result);
    }

    void addBodyCollision(const std::string& body_name, rbd::parsers::Visual visual)
    {
        collision_map[body_name].emplace_back(std::move(visual));
    }

    void
    addWorldToKinematicModel()
    {
        std::string world_body_name = "world";
        std::string world_joint_name = "world_joint";

        rbd::Body body(sva::RBInertiad(1., Eigen::Vector3d::Zero(), Eigen::Matrix3d::Identity()), world_body_name);
        mbg.addBody(body);
        // Create a fixed joint
        rbd::Joint joint(world_joint_name);
        mbg.addJoint(joint);

        mbg.linkBodies(world_body_name, sva::PTransformd::Identity(), mb.body(0).name(), sva::PTransformd::Identity(), world_joint_name);

        mb = mbg.makeMultiBody(world_body_name, true);

        updateLinkToParentJointMap();

        mbc = rbd::MultiBodyConfig(mb);
        mbc.zero(mb);
    }

    void createRobotCollisionObjects(std::vector<RobotCollisionObjectPtr>& rco_vec)
    {
        auto body_indexes = mb.bodyIndexByName();
        for (const auto& index : body_indexes)
        {
            auto collision = collision_map.find(index.first);
            if (collision != collision_map.end())
            {
                for (size_t i = 0; i < collision->second.size(); ++i)
                {
                    RobotCollisionObjectPtr rco = std::make_shared<RobotCollisionObject>();

                    rco->name() = collision->second[i].name;

                    // Type of the geometric shape
                    if (collision->second[i].geometry.type == rbd::parsers::Geometry::Type::MESH)
                    {
                        auto& mesh_data_rbd = boost::get<rbd::parsers::Geometry::Mesh>(collision->second[i].geometry.data);
                        rco->geometry() = rkcl::geometry::Mesh(mesh_data_rbd.filename, mesh_data_rbd.scale);
                    }
                    else if (collision->second[i].geometry.type == rbd::parsers::Geometry::Type::BOX)
                    {
                        auto& box_data_rbd = boost::get<rbd::parsers::Geometry::Box>(collision->second[i].geometry.data);
                        rco->geometry() = rkcl::geometry::Box(box_data_rbd.size);
                    }
                    else if (collision->second[i].geometry.type == rbd::parsers::Geometry::Type::CYLINDER)
                    {
                        auto& cylinder_data_rbd = boost::get<rbd::parsers::Geometry::Cylinder>(collision->second[i].geometry.data);
                        rco->geometry() = rkcl::geometry::Cylinder(cylinder_data_rbd.radius, cylinder_data_rbd.length);
                    }
                    else if (collision->second[i].geometry.type == rbd::parsers::Geometry::Type::SPHERE)
                    {
                        auto& sphere_data_rbd = boost::get<rbd::parsers::Geometry::Sphere>(collision->second[i].geometry.data);
                        rco->geometry() = rkcl::geometry::Sphere(sphere_data_rbd.radius);
                    }
                    else if (collision->second[i].geometry.type == rbd::parsers::Geometry::Type::SUPERELLIPSOID)
                    {
                        auto& superellipsoid_data_rbd = boost::get<rbd::parsers::Geometry::Superellipsoid>(collision->second[i].geometry.data);
                        rco->geometry() = rkcl::geometry::Superellipsoid(superellipsoid_data_rbd.size, superellipsoid_data_rbd.epsilon1, superellipsoid_data_rbd.epsilon2);
                    }
                    else
                    {
                        rco->geometry() = std::monostate{};
                    }

                    rco->linkName() = index.first;

                    Eigen::Affine3d origin;

                    origin.translation() = collision->second[i].origin.translation();
                    origin.linear() = collision->second[i].origin.rotation().transpose();

                    rco->origin() = origin;

                    rco_vec.push_back(rco);
                }
            }
        }
    }

    void checkJointTypes()
    {
        for (size_t i = 0; i < robot.jointGroupCount(); ++i)
        {
            auto joint_group = robot.jointGroup(i);
            for (const auto& joint_name : joint_group->jointNames())
            {
                auto joint = mbg.jointByName(joint_name);
                if ((joint->type() != rbd::Joint::Rev) && (joint->type() != rbd::Joint::Prism))
                    throw std::runtime_error("Only revolute and prismatic joints can be managed in RKCL, cannot consider joint " + joint_name);
            }
        }
    }

    void jointGroupIndices()
    {
        joint_group_index_map.clear();

        for (size_t i = 0; i < robot.jointGroupCount(); ++i)
        {
            auto joint_group = robot.jointGroup(i);
            std::vector<int> joint_group_indices;
            for (const auto& joint_name : joint_group->jointNames())
            {
                joint_group_indices.push_back(mb.sJointIndexByName(joint_name));
            }
            joint_group_index_map[joint_group->name()] = joint_group_indices;
        }
    }

    void initRobotJointLimits(const rbd::parsers::ParserResult& parser_result)
    {
        for (size_t i = 0; i < robot.jointGroupCount(); ++i)
        {
            auto joint_group = robot.jointGroup(i);

            if (not joint_group->limits().minPositionConfigured())
            {
                Eigen::VectorXd min_pos(joint_group->jointCount());
                for (size_t j = 0; j < joint_group->jointCount(); ++j)
                    min_pos(j) = parser_result.limits.lower.at(joint_group->jointNames()[j])[0];
                joint_group->limits().minPosition() = min_pos;
            }
            if (not joint_group->limits().maxPositionConfigured())
            {
                Eigen::VectorXd max_pos(joint_group->jointCount());
                for (size_t j = 0; j < joint_group->jointCount(); ++j)
                    max_pos(j) = parser_result.limits.upper.at(joint_group->jointNames()[j])[0];
                joint_group->limits().maxPosition() = max_pos;
            }
            if (not joint_group->limits().maxVelocityConfigured())
            {
                Eigen::VectorXd max_vel(joint_group->jointCount());
                for (size_t j = 0; j < joint_group->jointCount(); ++j)
                    max_vel(j) = parser_result.limits.velocity.at(joint_group->jointNames()[j])[0];
                joint_group->limits().maxVelocity() = max_vel;
            }
        }
    }

    void updateJointState()
    {
        for (size_t i = 0; i < robot.jointGroupCount(); ++i)
        {
            auto joint_group = robot.jointGroup(i);
            const auto& joint_group_index_vector = joint_group_index_map.find(joint_group->name())->second;
            std::lock_guard<std::mutex> lock(joint_group->state_mtx_);
            for (size_t j = 0; j < joint_group->jointCount(); ++j)
            {
                mbc.q[joint_group_index_vector[j]][0] = joint_group->state().position()(j);
                mbc.alpha[joint_group_index_vector[j]][0] = joint_group->state().velocity()(j);
                mbc.alphaD[joint_group_index_vector[j]][0] = joint_group->state().acceleration()(j);
            }
        }
    }

    void updateRobotCollisionObjectsPose(std::vector<RobotCollisionObjectPtr>& rco_vec)
    {
        for (auto& rco : rco_vec)
        {
            int index;
            try
            {
                index = mb.sBodyIndexByName(rco->linkName());
            }
            catch (...)
            {
                throw std::runtime_error(
                    "ForwardKinematicsRBDyn::updateRobotCollisionObjectsPose: Unable to find body " + rco->linkName());
            }

            auto link_pose = mbc.bodyPosW[index];
            Eigen::Affine3d eigen_link_pose;
            eigen_link_pose.translation() = link_pose.translation();
            eigen_link_pose.linear() = link_pose.rotation().transpose();

            rco->linkPose() = eigen_link_pose;
        }
    }

    void updateWorldCollisionObjectsPose(std::vector<CollisionObjectPtr>& wco_vec)
    {
        for (auto& wco : wco_vec)
        {
            int index;
            try
            {
                index = mb.sBodyIndexByName(wco->linkName());
            }
            catch (...)
            {
                throw std::runtime_error(
                    "ForwardKinematicsRBDyn::updateRobotCollisionObjectsPose: Unable to find body " + wco->linkName());
            }

            auto link_pose = mbc.bodyPosW[index];
            Eigen::Affine3d eigen_link_pose;
            eigen_link_pose.translation() = link_pose.translation();
            eigen_link_pose.linear() = link_pose.rotation().transpose();

            wco->poseWorld() = eigen_link_pose;
        }
    }

    void updatePointPose(rkcl::ObservationPointPtr point, const int& point_body_index, const int& point_ref_body_index)
    {
        // update point pose
        auto tcp_pose = mbc.bodyPosW[point_body_index];
        Eigen::Affine3d eigen_tcp_pose;
        eigen_tcp_pose.translation() = tcp_pose.translation();
        eigen_tcp_pose.linear() = tcp_pose.rotation().transpose();

        if (point->refBodyName() != "world")
        {
            auto ref_body_pose = mbc.bodyPosW[point_ref_body_index];
            Eigen::Affine3d eigen_ref_body_pose;
            eigen_ref_body_pose.translation() = ref_body_pose.translation();
            eigen_ref_body_pose.linear() = ref_body_pose.rotation().transpose();
            eigen_tcp_pose = eigen_ref_body_pose.inverse() * eigen_tcp_pose;
            point->_refBodyPoseWorld() = eigen_ref_body_pose;
        }
        else
        {
            point->_refBodyPoseWorld().matrix().setIdentity();
        }
        std::lock_guard<std::mutex> lock(point->state_mtx_);
        point->_state().pose() = eigen_tcp_pose;
    }

    void updatePointTwist(rkcl::ObservationPointPtr point, const int& point_body_index, const int& point_ref_body_index)
    {
        auto tcp_twist = mbc.bodyVelW[point_body_index];

        Eigen::Matrix<double, 6, 1> eigen_tcp_twist;
        eigen_tcp_twist.head<3>() = tcp_twist.linear();
        eigen_tcp_twist.tail<3>() = tcp_twist.angular();

        if (point->refBodyName() != "world")
        {
            auto ref_body_twist = mbc.bodyVelW[point_ref_body_index];

            eigen_tcp_twist.head<3>() = (eigen_tcp_twist.head<3>() - ref_body_twist.linear()).eval();
            eigen_tcp_twist.tail<3>() = (eigen_tcp_twist.tail<3>() - ref_body_twist.angular()).eval();

            auto ref_body_pose = mbc.bodyPosW[point_ref_body_index];
            Eigen::Affine3d eigen_ref_body_pose;
            eigen_ref_body_pose.translation() = ref_body_pose.translation();
            eigen_ref_body_pose.linear() = ref_body_pose.rotation().transpose();

            eigen_tcp_twist.head<3>() = (eigen_ref_body_pose.linear().transpose() * eigen_tcp_twist.head<3>()).eval();
            eigen_tcp_twist.tail<3>() = (eigen_ref_body_pose.linear().transpose() * eigen_tcp_twist.tail<3>()).eval();
        }

        std::lock_guard<std::mutex> lock(point->state_mtx_);
        point->_state().twist() = eigen_tcp_twist;
    }

    void updatePointAcceleration(rkcl::ObservationPointPtr point, const int& point_body_index, const int& point_ref_body_index)
    {
        // Compute acceleration expressed in world frame from acceleration in body frame
        sva::PTransformd E_0_i(mbc.bodyPosW[point_body_index].rotation());
        auto tcp_acc = E_0_i.invMul(mbc.bodyAccB[point_body_index]);

        Eigen::Matrix<double, 6, 1> eigen_tcp_acc;
        eigen_tcp_acc.head<3>() = tcp_acc.linear();
        eigen_tcp_acc.tail<3>() = tcp_acc.angular();

        if (point->refBodyName() != "world")
        {
            auto ref_body_pose = mbc.bodyPosW[point_ref_body_index];
            Eigen::Affine3d eigen_ref_body_pose;
            eigen_ref_body_pose.translation() = ref_body_pose.translation();
            eigen_ref_body_pose.linear() = ref_body_pose.rotation().transpose();

            eigen_tcp_acc.head<3>() = eigen_ref_body_pose.linear().transpose() * eigen_tcp_acc.head<3>();
            eigen_tcp_acc.tail<3>() = eigen_ref_body_pose.linear().transpose() * eigen_tcp_acc.tail<3>();
        }

        std::lock_guard<std::mutex> lock(point->state_mtx_);
        point->_state().acceleration() = eigen_tcp_acc;
    }

    void updateControlPointJacobian(rkcl::ControlPointPtr point, const int& point_ref_body_index)
    {
        auto point_const = static_cast<rkcl::ControlPointConstPtr>(point);
        rbd::Jacobian rbd_jac;
        if (point_const->rootBodyName() == "world")
        {
            rbd_jac = rbd::Jacobian(mb, point_const->bodyName());
        }
        else
        {
            rbd_jac = rbd::Jacobian(mb, point_const->bodyName(), point_const->rootBodyName());
        }
        Eigen::MatrixXd jac_mat = rbd_jac.jacobian(mb, mbc);
        if (point_const->rootBodyName() != point_const->refBodyName())
        {
            auto root_body_index = mb.sBodyIndexByName(point_const->rootBodyName());
            auto root_body_pose = mbc.bodyPosW[root_body_index];
            auto ref_body_pose = mbc.bodyPosW[point_ref_body_index];
            auto ref_R_root = ref_body_pose.rotation() * root_body_pose.rotation().transpose();
            Eigen::Matrix6d ref_R_root_augm;
            ref_R_root_augm << ref_R_root, Eigen::Matrix3d::Zero(), Eigen::Matrix3d::Zero(), ref_R_root;
            jac_mat = (ref_R_root_augm * jac_mat).eval();
        }

        auto joints_path = controllableJointsPath(rbd_jac.jointsPath());
        point->_kinematics()._jointGroupJacobian() = jointGroupJacobian(jac_mat, joints_path);

        if (jacobian_derivative_enabled)
        {
            if (point_const->rootBodyName() == "world")
            {
                Eigen::MatrixXd jac_dot_mat = rbd_jac.jacobianDot(mb, mbc);
                point->_kinematics()._jointGroupDerivativeJacobian() = jointGroupJacobian(jac_dot_mat, joints_path);
            }
            else
            {
                std::cerr << "Computing derivative Jacobian for ref body != 'world' is not currently possible, aborting \n";
            }
        }

        // If joint group control names have never been set or root body name has changed since last check
        if (point->kinematics().jointGroupControlNames().empty() or cp_root_body_name_map.find(point)->second != point->rootBodyName())
            updateJointGroupControlNames(point, joints_path);
    }

    std::vector<int> controllableJointsPath(const std::vector<int>& joints_path)
    {
        std::vector<int> controllable_joints_path;
        for (size_t i = 0; i < joints_path.size(); ++i)
        {
            for (size_t j = 0; j < mb.joint(joints_path[i]).dof(); ++j)
                controllable_joints_path.push_back(joints_path[i]);
        }
        return controllable_joints_path;
    }

    void updateObservationPointsKinematicData()
    {
        for (size_t i = 0; i < robot.observationPointCount(); ++i)
        {
            auto op = robot.observationPoint(i);

            int op_body_index;
            try
            {
                op_body_index = mb.sBodyIndexByName(op->bodyName());
            }
            catch (...)
            {
                throw std::runtime_error(
                    "ForwardKinematicsRBDyn::forwardKinematics: Unable to find body " + op->bodyName());
            }

            int op_ref_body_index;
            if (op->refBodyName().empty())
            {
                op_ref_body_index = -1;
            }
            else
            {
                try
                {
                    op_ref_body_index = mb.sBodyIndexByName(op->refBodyName());
                }
                catch (...)
                {
                    throw std::runtime_error(
                        "ForwardKinematicsRBDyn::forwardKinematics: Unable to find body " + op->refBodyName());
                }
            }

            updatePointPose(op, op_body_index, op_ref_body_index);
            if (forward_velocity_enabled)
                updatePointTwist(op, op_body_index, op_ref_body_index);
            if (forward_acceleration_enabled)
                updatePointAcceleration(op, op_body_index, op_ref_body_index);
        }
    }

    void updateControlPointsKinematicData()
    {
        for (size_t i = 0; i < robot.controlPointCount(); ++i)
        {
            auto cp = robot.controlPoint(i);

            int cp_body_index;

            try
            {
                cp_body_index = mb.sBodyIndexByName(robot.controlPoint(i)->bodyName());
            }
            catch (...)
            {
                throw std::runtime_error(
                    "ForwardKinematicsRBDyn::forwardKinematics: Unable to find body " + robot.controlPoint(i)->bodyName());
            }

            int cp_ref_body_index;

            try
            {
                cp_ref_body_index = mb.sBodyIndexByName(robot.controlPoint(i)->refBodyName());
            }
            catch (...)
            {
                throw std::runtime_error(
                    "ForwardKinematicsRBDyn::forwardKinematics: Unable to find body " + robot.controlPoint(i)->refBodyName());
            }

            updatePointPose(cp, cp_body_index, cp_ref_body_index);
            if (forward_velocity_enabled)
                updatePointTwist(cp, cp_body_index, cp_ref_body_index);
            if (forward_acceleration_enabled)
                updatePointAcceleration(cp, cp_body_index, cp_ref_body_index);

            updateControlPointJacobian(cp, cp_ref_body_index);
        }
    }

    void forwardKinematics()
    {
        updateModel();
        updateObservationPointsKinematicData();
        updateControlPointsKinematicData();
    }

    void updateModel()
    {
        updateJointState();
        rbd::forwardKinematics(mb, mbc);
        if (forward_velocity_enabled)
            rbd::forwardVelocity(mb, mbc);
        if (forward_acceleration_enabled)
            rbd::forwardAcceleration(mb, mbc);
    }

    void computeCollisionAvoidanceConstraints(const std::vector<RobotCollisionObjectPtr>& rco_vec)
    {
        // See "A local based approach for path planning of manipulators with a high
        // number of degrees of freedom" and
        // "Bimanual Assembly of Two Parts with Relative Motion Generation and Task
        // Related Optimization"
        size_t new_constraints_count = 0;
        std::vector<size_t> existing_constraints_count;
        for (const auto& rco : rco_vec)
        {
            new_constraints_count += rco->selfCollisionPreventEvals().size();
            new_constraints_count += rco->worldCollisionPreventEvals().size();
        }

        for (size_t i = 0; i < robot.jointGroupCount(); ++i)
        {
            existing_constraints_count.push_back(robot.jointGroup(i)->internalConstraints().matrixInequality().rows());
            robot.jointGroup(i)->_internalConstraints().matrixInequality().resize(existing_constraints_count[i] + new_constraints_count, robot.jointGroup(i)->jointCount());
        }

        size_t constraint_index = 0;
        for (const auto& rco : rco_vec)
        {
            for (size_t i = 0; i < rco->selfCollisionPreventEvals().size(); ++i)
            {
                // Contribution : Necessary to consider the point on the object for
                // which the jacobian is computed !!
                Eigen::Vector3d witness_point_in_body_frame = rco->linkPose()->inverse() * rco->selfCollisionPreventEvals()[i].witnessPoint();
                rbd::Jacobian rbd_jac = rbd::Jacobian(mb, rco->linkName(), rco->selfCollisionPreventEvals()[i].otherLinkName(), witness_point_in_body_frame);
                Eigen::MatrixXd jac_mat = rbd_jac.jacobian(mb, mbc);
                auto joints_path = controllableJointsPath(rbd_jac.jointsPath());

                for (size_t j = 0; j < robot.jointGroupCount(); ++j)
                {
                    auto joint_group_jac_mat = jointGroupJacobian(jac_mat, joints_path, robot.jointGroup(j)->name());
                    // We keep only the  translational part of the jacobian
                    Eigen::MatrixXd mat = joint_group_jac_mat.block(0, 0, 3, joint_group_jac_mat.cols());
                    joint_group_jac_mat = mat;
                    // Compute normalized vector aligning the two points
                    Eigen::Vector3d n = (rco->selfCollisionPreventEvals()[i].otherWitnessPoint() - rco->selfCollisionPreventEvals()[i].witnessPoint()) /
                                        (rco->selfCollisionPreventEvals()[i].otherWitnessPoint() - rco->selfCollisionPreventEvals()[i].witnessPoint()).norm();

                    // Express n in ref body frame
                    int other_body_index;
                    try
                    {
                        other_body_index = mb.sBodyIndexByName(rco->selfCollisionPreventEvals()[i].otherLinkName());
                    }
                    catch (...)
                    {
                        throw std::runtime_error(
                            "ForwardKinematicsRBDyn::forwardKinematics: Unable to find body " + rco->selfCollisionPreventEvals()[i].otherLinkName());
                    }

                    n = mbc.bodyPosW[other_body_index].rotation() * n;

                    // Compute jacobian expressing the distance variations
                    joint_group_jac_mat = n.transpose() * joint_group_jac_mat;
                    robot.jointGroup(j)->_internalConstraints().matrixInequality().block(existing_constraints_count[j] + constraint_index, 0, 1, robot.jointGroup(j)->jointCount()) = joint_group_jac_mat;
                }
                ++constraint_index;
            }
            for (size_t i = 0; i < rco->worldCollisionPreventEvals().size(); ++i)
            {
                Eigen::Vector3d witness_point_in_body_frame = rco->linkPose()->inverse() * rco->worldCollisionPreventEvals()[i].witnessPoint();
                rbd::Jacobian rbd_jac = rbd::Jacobian(mb, rco->linkName(), witness_point_in_body_frame);
                Eigen::MatrixXd jac_mat = rbd_jac.jacobian(mb, mbc);
                auto joints_path = controllableJointsPath(rbd_jac.jointsPath());

                for (size_t j = 0; j < robot.jointGroupCount(); ++j)
                {
                    auto joint_group_jac_mat = jointGroupJacobian(jac_mat, joints_path, robot.jointGroup(j)->name());
                    // We keep only the  translational part of the jacobian
                    Eigen::MatrixXd mat = joint_group_jac_mat.block(0, 0, 3, joint_group_jac_mat.cols());
                    joint_group_jac_mat = mat;
                    // Compute normalized vector aligning the two points
                    Eigen::Vector3d n = (rco->worldCollisionPreventEvals()[i].worldWitnessPoint() - rco->worldCollisionPreventEvals()[i].witnessPoint()) /
                                        (rco->worldCollisionPreventEvals()[i].worldWitnessPoint() - rco->worldCollisionPreventEvals()[i].witnessPoint()).norm();

                    // Compute jacobian expressing the distance variations
                    joint_group_jac_mat = n.transpose() * joint_group_jac_mat;
                    robot.jointGroup(j)->_internalConstraints().matrixInequality().block(existing_constraints_count[j] + constraint_index, 0, 1, robot.jointGroup(j)->jointCount()) = joint_group_jac_mat;
                }
                ++constraint_index;
            }
        }
    }

    // std::vector<std::vector<Eigen::MatrixXd>> computeSampledRobotCollisionObjectJacobians(const std::vector<RobotCollisionObjectPtr>& rco_vec)
    // {
    //     // Assume that all sampled rco belonging to rco_vec are from the same link
    //     // TODO: to be checked
    //     std::vector<std::vector<Eigen::MatrixXd>> rco_jac_vec_vec;

    //     rbd::Jacobian rbd_jac = rbd::Jacobian(mb, rco_vec[0]->linkName());

    //     for (const auto& rco : rco_vec)
    //     {
    //         std::vector<Eigen::MatrixXd> rco_jac_vec;
    //         rbd_jac.point(rco->origin()->translation());

    //         Eigen::MatrixXd jac_mat = rbd_jac.jacobian(mb, mbc);
    //         auto joints_path = controllableJointsPath(rbd_jac.jointsPath());

    //         for (size_t j = 0; j < robot.jointGroupCount(); ++j)
    //         {
    //             auto joint_group_jac_mat = jointGroupJacobian(jac_mat, joints_path, robot.jointGroup(j)->name());

    //             rco_jac_vec.emplace_back(joint_group_jac_mat);
    //         }
    //         rco_jac_vec_vec.emplace_back(rco_jac_vec);
    //     }

    //     return rco_jac_vec_vec;
    // }

    void computeGuidanceTaskJacobian(ControlPointPtr guidance_control_point, RobotCollisionObjectPtr guidance_rco)
    {
        const auto& world_collision_attractive_eval = guidance_rco->worldCollisionAttractiveEvals().at(0);
        Eigen::Vector3d witness_point_in_body_frame = guidance_rco->linkPose()->inverse() * world_collision_attractive_eval.witnessPoint();
        rbd::Jacobian rbd_jac = rbd::Jacobian(mb, guidance_rco->linkName(), witness_point_in_body_frame);
        Eigen::MatrixXd jac_mat = rbd_jac.jacobian(mb, mbc);
        auto joints_path = controllableJointsPath(rbd_jac.jointsPath());

        guidance_control_point->_kinematics()._jointGroupJacobian() = jointGroupJacobian(jac_mat, joints_path);
        // If joint group control names have never been set or root body name has changed since last check
        if (guidance_control_point->kinematics().jointGroupControlNames().empty() or cp_root_body_name_map.find(guidance_control_point)->second != guidance_control_point->rootBodyName())
            updateJointGroupControlNames(guidance_control_point, joints_path);
    }

    void addFixedLink(const std::string& link_name, const std::string& parent_link_name, const Eigen::Affine3d& pose)
    {
        rbd::Body body(sva::RBInertiad(1., Eigen::Vector3d::Zero(), Eigen::Matrix3d::Identity()), link_name);
        mbg.addBody(body);
        // Create a fixed joint
        std::string joint_name = link_name + "__joint";
        rbd::Joint joint(joint_name);
        mbg.addJoint(joint);

        mbg.linkBodies(parent_link_name, sva::PTransformd(pose.rotation().transpose(), pose.translation()), link_name, sva::PTransformd::Identity(), joint_name);

        // Store the current mb tranforms and indexes
        std::vector<sva::PTransformd> transforms(mb.transforms());
        std::unordered_map<std::string, int> body_indexes_by_name(mb.bodyIndexByName());
        mb = mbg.makeMultiBody(mb.body(0).name(), true);
        updateLinkToParentJointMap();

        // Copy transforms from the previous mb to the new one
        for (const auto& body_index_by_name : body_indexes_by_name)
        {
            auto new_index = mb.bodyIndexByName(body_index_by_name.first);
            mb.transform(new_index, transforms[body_index_by_name.second]);
        }

        mbc = rbd::MultiBodyConfig(mb);
        mbc.zero(mb);

        jointGroupIndices();
    }

    void setFixedLinkPose(const std::string& link_name, const Eigen::Affine3d& pose)
    {
        const int joint_idx = link_to_parent_joint.at(link_name);
        mb.transform(joint_idx, sva::PTransformd(pose.rotation().transpose(), pose.translation()));
    }

    void updateJointGroupControlNames(const rkcl::ControlPointPtr& point, const std::vector<int>& dof_path)
    {
        point->_kinematics()._jointGroupControlNames().clear();
        cp_root_body_name_map[point] = point->rootBodyName();
        for (const auto& joint_group_index : joint_group_index_map)
        {
            for (size_t i = 0; i < joint_group_index.second.size(); ++i)
            {
                auto index_pos = std::find(dof_path.begin(), dof_path.end(), joint_group_index.second[i]) - dof_path.begin();
                if (index_pos < dof_path.size()) // index found
                {
                    point->_kinematics()._jointGroupControlNames().push_back(joint_group_index.first);
                    break;
                }
            }
        }
    }

    std::unordered_map<std::string, Eigen::MatrixXd> jointGroupJacobian(Eigen::MatrixXd& jac_mat, std::vector<int>& dof_path)
    {
        std::unordered_map<std::string, Eigen::MatrixXd> joint_group_jacobian;

        for (const auto& joint_group_index : joint_group_index_map)
        {
            Eigen::MatrixXd joint_group_jac = Eigen::MatrixXd::Zero(6, joint_group_index.second.size());
            for (size_t i = 0; i < joint_group_index.second.size(); ++i)
            {
                auto index_pos = std::find(dof_path.begin(), dof_path.end(), joint_group_index.second[i]) - dof_path.begin();
                if (index_pos < dof_path.size()) // index found
                {
                    // Translations and rotations are reversed, so swap
                    joint_group_jac.block(0, i, 3, 1) = jac_mat.block(3, index_pos, 3, 1);
                    joint_group_jac.block(3, i, 3, 1) = jac_mat.block(0, index_pos, 3, 1);
                }
            }
            joint_group_jacobian[joint_group_index.first] = joint_group_jac;
        }

        return joint_group_jacobian;
    }

    Eigen::MatrixXd jointGroupJacobian(Eigen::MatrixXd& jac_mat, std::vector<int>& dof_path, const std::string& joint_group_name)
    {
        const auto& joint_group_index = joint_group_index_map.find(joint_group_name);

        Eigen::MatrixXd joint_group_jac = Eigen::MatrixXd::Zero(6, joint_group_index->second.size());
        for (size_t i = 0; i < joint_group_index->second.size(); ++i)
        {
            auto index_pos = std::find(dof_path.begin(), dof_path.end(), joint_group_index->second[i]) - dof_path.begin();
            if (index_pos < dof_path.size()) // index found
            {
                // Translations and rotations are reversed, so swap
                joint_group_jac.block(0, i, 3, 1) = jac_mat.block(3, index_pos, 3, 1);
                joint_group_jac.block(3, i, 3, 1) = jac_mat.block(0, index_pos, 3, 1);
            }
        }
        return joint_group_jac;
    }

    Eigen::Affine3d getLinkPoseInWorld(const std::string& link_name) const
    {
        Eigen::Affine3d ei_pose;
        auto link_pose = mbc.bodyPosW[mb.bodyIndexByName(link_name)];
        ei_pose.translation() = link_pose.translation();
        ei_pose.linear() = link_pose.rotation().transpose();
        return ei_pose;
    }

    void updateLinkToParentJointMap()
    {
        auto link_to_parent_joint_name = mbg.predecessorJoint("world");
        link_to_parent_joint.clear();
        for (const auto& [link, joint] : link_to_parent_joint_name)
        {
            link_to_parent_joint[link] = mb.jointIndexByName(joint);
        }
    }

    bool hasLink(const std::string& name) const
    {
        return std::find_if(mb.bodies().begin(), mb.bodies().end(),
                            [this, &name](const rbd::Body& body)
                            {
                                return body.name() == name;
                            }) != mb.bodies().end();
    }

    std::string getLinkParentName(const std::string& link) const
    {
        const auto parent_idx = mb.parent(mb.bodyIndexByName(link));
        return mb.body(parent_idx).name();
    }

    rbd::MultiBody mb;
    rbd::MultiBodyConfig mbc;
    rbd::MultiBodyGraph mbg;
    std::map<std::string, std::vector<rbd::parsers::Visual>> collision_map;

    std::vector<int> path_joint_indexes;

    std::map<std::string, int> link_to_parent_joint;

    Robot& robot;
    std::unordered_map<std::string, std::vector<int>> joint_group_index_map;

    std::unordered_map<rkcl::ControlPointPtr, std::string> cp_root_body_name_map;

    bool forward_velocity_enabled, forward_acceleration_enabled, jacobian_derivative_enabled;
};

ForwardKinematicsRBDyn::ForwardKinematicsRBDyn(Robot& robot, const std::string& model, rbd::parsers::ParserInput input)
    : impl_(std::make_unique<ForwardKinematicsRBDyn::pImpl>(robot, model, input))
{
}

ForwardKinematicsRBDyn::ForwardKinematicsRBDyn(Robot& robot, const YAML::Node& configuration)
{
    if (configuration)
    {
        std::string model;
        rbd::parsers::ParserInput input;
        if (auto path = configuration["path"]; path)
        {
            model = path.as<std::string>();
            input = rbd::parsers::ParserInput::File;
        }
        else if (auto description = configuration["description"]; description)
        {
            model = description.as<std::string>();
            input = rbd::parsers::ParserInput::Description;
        }
        else
        {
            throw std::runtime_error(
                "ForwardKinematicsRBDyn::ForwardKinematicsRBDyn: You must provide a "
                "'path' or 'description' field in the configuration.");
        }

        impl_ = std::make_unique<ForwardKinematicsRBDyn::pImpl>(robot, model, input);
        configure(configuration);
    }
    else
    {
        throw std::runtime_error(
            "ForwardKinematicsRBDyn::ForwardKinematicsRBDyn: Unable to configure");
    }
}

bool ForwardKinematicsRBDyn::configure(const YAML::Node& configuration)
{
    if (configuration)
    {
        if (auto forward_velocity_enabled = configuration["forward_velocity_enabled"]; forward_velocity_enabled)
        {
            impl_->forward_velocity_enabled = forward_velocity_enabled.as<bool>();
        }

        if (auto forward_acceleration_enabled = configuration["forward_acceleration_enabled"]; forward_acceleration_enabled)
        {
            impl_->forward_acceleration_enabled = forward_acceleration_enabled.as<bool>();
        }

        if (auto jacobian_derivative_enabled = configuration["jacobian_derivative_enabled"]; jacobian_derivative_enabled)
        {
            impl_->jacobian_derivative_enabled = jacobian_derivative_enabled.as<bool>();
        }

        if (auto world_pose = configuration["world_pose"]; world_pose)
        {
            auto world_pose_array = world_pose.as<std::vector<double>>();
            sva::PTransformd pose;
            for (size_t i = 0; i < 3; ++i)
            {
                pose.translation()(i) = world_pose_array[i];
            }
            pose.rotation() = Eigen::AngleAxisd(world_pose_array[3], Eigen::Vector3d::UnitX()) *
                              Eigen::AngleAxisd(world_pose_array[4], Eigen::Vector3d::UnitY()) *
                              Eigen::AngleAxisd(world_pose_array[5], Eigen::Vector3d::UnitZ());
            pose.rotation() = pose.rotation().transpose().eval();
            impl_->mb.transform(1, pose);
        }

        if (auto fixed_links = configuration["fixed_links"]; fixed_links)
        {
            for (const auto& fixed_link : fixed_links)
            {
                std::string parent_link_name, link_name;

                if (fixed_link["link_name"])
                {
                    link_name = fixed_link["link_name"].as<std::string>();
                }
                else
                {
                    throw std::runtime_error(
                        "ForwardKinematicsRBDyn::ForwardKinematicsRBDyn: You must "
                        "provide a 'link_name' for each fixed link");
                }

                const bool link_exists = impl_->hasLink(link_name);

                if (auto parent_link_name_conf = fixed_link["parent_link_name"]; parent_link_name_conf)
                {
                    parent_link_name = fixed_link["parent_link_name"].as<std::string>("world");

                    if (link_exists and impl_->getLinkParentName(link_name) != parent_link_name)
                    {
                        throw std::runtime_error(
                            "ForwardKinematicsRBDyn::ForwardKinematicsRBDyn: You cannot "
                            "change a fixed link parent");
                    }
                }
                else
                {
                    parent_link_name = "world";
                }

                if (not link_exists)
                {
                    addFixedLink(link_name, parent_link_name, Eigen::Affine3d::Identity());
                }

                if (auto pose_node = fixed_link["pose"]; pose_node)
                {
                    if (pose_node.IsSequence())
                    {
                        auto pose_vec = fixed_link["pose"].as<std::vector<double>>();
                        if (pose_vec.size() != 6)
                        {
                            throw std::runtime_error(
                                "ForwardKinematicsRBDyn::ForwardKinematicsRBDyn: pose of fixed "
                                "links must have six components [tx(m), ty(m), tz(m), rx(rad), "
                                "ry(rad), rz(rad)]");
                        }

                        Eigen::Affine3d pose;
                        for (size_t i = 0; i < 3; ++i)
                        {
                            pose.translation()(i) = pose_vec[i];
                        }
                        pose.linear() = (Eigen::AngleAxisd(pose_vec[3], Eigen::Vector3d::UnitX()) *
                                         Eigen::AngleAxisd(pose_vec[4], Eigen::Vector3d::UnitY()) *
                                         Eigen::AngleAxisd(pose_vec[5], Eigen::Vector3d::UnitZ()))
                                            .matrix();

                        setFixedLinkPose(link_name, pose);
                    }
                    else
                    {
                        const auto other_link = pose_node.as<std::string>();
                        setFixedLinkPose(link_name, getLinkPose(other_link, impl_->getLinkParentName(link_name)));
                    }
                }
                else
                {
                    throw std::runtime_error(
                        "ForwardKinematicsRBDyn::ForwardKinematicsRBDyn: You must "
                        "provide a 'pose' for each fixed link");
                }

                if (auto collision_node = fixed_link["collision"]; collision_node)
                {
                    Eigen::Vector3d xyz;
                    Eigen::Vector3d rpy;
                    for (auto collision : collision_node)
                    {
                        rbd::parsers::Visual collision_data;
                        collision_data.name = collision["name"].as<std::string>();

                        parseFrame(collision["frame"], collision_data.name, xyz, rpy);
                        collision_data.origin.rotation() = MatrixFromRPY(rpy[0], rpy[1], rpy[2]);
                        collision_data.origin.translation() = xyz;

                        if (parseGeometry(collision["geometry"], collision_data.geometry))
                        {
                            impl_->addBodyCollision(link_name, std::move(collision_data));
                        }
                    }
                }
            }
        }
        // process();
    }
    return true;
}

ForwardKinematicsRBDyn::~ForwardKinematicsRBDyn() = default;

bool ForwardKinematicsRBDyn::process()
{
    impl_->forwardKinematics();
    return true;
}

void ForwardKinematicsRBDyn::updateModel()
{
    impl_->updateModel();
}

void ForwardKinematicsRBDyn::createRobotCollisionObjects(std::vector<RobotCollisionObjectPtr>& rco_vec)
{
    impl_->createRobotCollisionObjects(rco_vec);
}
void ForwardKinematicsRBDyn::updateRobotCollisionObjectsPose(std::vector<RobotCollisionObjectPtr>& rco_vec)
{
    impl_->updateRobotCollisionObjectsPose(rco_vec);
}

void ForwardKinematicsRBDyn::updateWorldCollisionObjectsPose(std::vector<CollisionObjectPtr>& wco_vec)
{
    impl_->updateWorldCollisionObjectsPose(wco_vec);
}

void ForwardKinematicsRBDyn::computeCollisionAvoidanceConstraints(const std::vector<RobotCollisionObjectPtr>& rco_vec)
{
    impl_->computeCollisionAvoidanceConstraints(rco_vec);
}

void ForwardKinematicsRBDyn::computeGuidanceTaskJacobian(ControlPointPtr guidance_control_point, RobotCollisionObjectPtr guidance_rco)
{
    impl_->computeGuidanceTaskJacobian(guidance_control_point, guidance_rco);
}

void ForwardKinematicsRBDyn::addFixedLink(const std::string& link_name,
                                          const std::string& parent_link_name,
                                          const Eigen::Affine3d& pose)
{
    impl_->addFixedLink(link_name, parent_link_name, pose);
}

void ForwardKinematicsRBDyn::setFixedLinkPose(const std::string& link_name, const Eigen::Affine3d& pose)
{
    impl_->setFixedLinkPose(link_name, pose);
}

Eigen::Affine3d ForwardKinematicsRBDyn::getLinkPose(const std::string& link_name, const std::string& reference_link_name) const
{
    if (reference_link_name == "world")
    {
        return impl_->getLinkPoseInWorld(link_name);
    }
    else
    {
        auto link_in_world = impl_->getLinkPoseInWorld(link_name);
        auto ref_in_world = impl_->getLinkPoseInWorld(reference_link_name);
        return ref_in_world.inverse() * link_in_world;
    }
}