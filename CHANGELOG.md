# [](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v2.1.0...v) (2022-07-21)



# [2.1.0](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v2.0.0...v2.1.0) (2022-02-17)


### Features

* collision object adaptation ([55cdf0d](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/55cdf0d7f04df6ee0d0ab7fa2246e5a63a94fbc5))
* disable derivative jacobian computation by default ([a07c87c](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/a07c87cdbaf6e72f820db71380c402cb6fcbedfd))
* split process function to call updateModel ([ba1eebf](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/ba1eebf81d28471302a6c872b4e9d2a7cd6fbea5))
* update dep to rkcl-core ([e949a66](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/e949a662922730d498de241a7925fdc0a5b434d6))



# [2.0.0](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v1.1.1...v2.0.0) (2021-10-01)


### Bug Fixes

* computation of point twist ([9dbfb7b](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/9dbfb7b98bfcd08f09c708e0e9c7d18ee11192bf))
* incorrect YAML error message ([d6bff7e](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/d6bff7eb57f8c029e897752ecce283c8aa321203))
* use eval() in eigen operations ([95d9598](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/95d95982f7740a7b873ba80694d406ef1cd28df3))


### Features

* ability to pass a yaml/urdf description instead of a file path ([6a596bc](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/6a596bc7c9a5fbc349bcd6b6f15b0ca700902e9a))
* ability to reconfigure fixed links with a new pose ([4ebe39e](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/4ebe39e36e97a56e6072e5cad4f057546c5b4873))
* added guidance task jacobian computation ([44c4b91](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/44c4b914334561e0d285743d077ee1ba2d958796))
* disable forward velocity by default as it is estimated in robot class ([b246296](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/b2462969399e5716870fe4bc48374a3ded7addbb))
* implement new ForwardKinematics interface ([4930e6f](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/4930e6fe9fd08034b2153323b14603878c553754))
* use conventional commits ([fb5b04c](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/commits/fb5b04cbe2e4c5fe3ad47694521de06ba218820a))



## [1.1.1](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v1.1.0...v1.1.1) (2020-10-13)



# [1.1.0](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v1.0.4...v1.1.0) (2020-06-17)



## [1.0.4](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v1.0.3...v1.0.4) (2020-03-31)



## [1.0.3](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v1.0.2...v1.0.3) (2020-03-23)



## [1.0.2](https://gite.lirmm.fr/rkcl/rkcl-fk-rbdyn/compare/v1.0.1...v1.0.2) (2020-03-05)



## 1.0.1 (2020-02-20)



